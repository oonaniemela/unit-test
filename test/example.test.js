const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined;
    
    before(() => {
        myvar = 1;
        console.log('This text is before texting')
    })
    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1)
        expect(result).to.equal(2)
    })
    it('Should return 5 when using subtraction function with x=5, y=5, z=5', () => {
        const result = mylib.subtraction(5, 5, 5)
        expect(result).to.equal(5)
    })
    it.skip('Assert foo is not bar', () => {
        assert('foo' !== 'bar')
    })
    it('Myvar should exist', () => {
        should.exist(myvar)
    })
    after(() => {
        console.log('This text is after testing')
    })
})